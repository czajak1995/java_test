Usage:
java Main filename

where filename is name of the file where you want to save database records.

There are three option available:
quit - exit the program
add - add new user to the file
delete - delete existing user from file