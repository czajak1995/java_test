import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static final String FILENAME = "users.txt";

    public static void showHelp() {
        System.out.println(
                "*************************\n" +
                "* Options:              *\n" +
                "* add - add user        *\n" +
                "* delete - delete user  *\n" +
                "* quit - exit app       *\n" +
                "*************************");
    }

    public static void showPrompt() {
        System.out.print("> ");
    }


    public static void main(String [] args) {

        if(args.length == 0) {
            System.out.println("Usage:\n" +
                    "java Main filename\n");
            return;
        }

        FileParser fileParser = new FileParser(args[0]);
        String option = "";
        Scanner reader = new Scanner(System.in);
        String name;
        int age;

        while(!option.equals("quit")) {
            showHelp();
            showPrompt();
            option = reader.nextLine();
            try {
                switch (option) {
                    case "add":
                        System.out.println("Pass user name");
                        showPrompt();
                        name = reader.nextLine();
                        System.out.println("Pass user age");
                        showPrompt();
                        age = reader.nextInt();
                        reader.nextLine();
                        fileParser.addUser(name, age);
                        break;
                    case "delete":
                        System.out.println("Pass user name");
                        showPrompt();
                        name = reader.nextLine();
                        fileParser.deleteUser(name);
                        break;
                    default:
                        break;
                }
            } catch(InputMismatchException e) {
                System.out.println("Passed data are incorrect");
            }
        }

        reader.close();
        fileParser.close();
    }

}
