import java.io.*;

public class FileParser {

    private PrintWriter writer;
    private BufferedReader reader;
    private final String fileName;
    private File file;

    public FileParser(String fileName) {
        this.fileName = fileName;
        file = new File(fileName);
        try {
            writer = new PrintWriter(new FileWriter(file, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.close();
    }

    private boolean isUserInFile(String name) {
        String currentRecord;

        boolean exist = false;
        try {
            reader = new BufferedReader(new FileReader(file));
            while((currentRecord = reader.readLine()) != null) {
                String currentName = currentRecord.split(" ")[0];
                if (currentName.equals(name)) {
                    exist = true;
                    break;
                }
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Couldn't read file \"" + fileName + "\"");
        }



        return exist;
    }


    public void addUser(String name, int age) {

        try {
            writer = new PrintWriter(new FileWriter(file, true));

            if (!isUserInFile(name)) {
                writer.println(name + " " + age);
                System.out.println("User \"" + name + "\" added succesfully");
            } else {
                System.out.println("User \"" + name + "\" exists in database");
            }

            writer.close();

        } catch (IOException e) {
            System.out.println("Couldn't write to file \"" + fileName + "\"");
        }



    }

    public void deleteUser(String name) {
        File tempFile = new File("~" + fileName);
        String currentRecord;
        boolean changed = false;
        try {
            reader = new BufferedReader(new FileReader(file));
            PrintWriter tempWriter = new PrintWriter(new FileWriter(tempFile, true));
            while((currentRecord = reader.readLine()) != null) {
                String currentName = currentRecord.split(" ")[0];
                if (!currentName.equals(name)) {
                    tempWriter.println(currentRecord);
                } else {
                    changed = true;
                }
            }
            tempWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!changed) {
            System.out.println("User \"" + name + "\" not exists in database");
        } else if(tempFile.renameTo(new File(fileName)) && changed) {
            System.out.println("User \"" + name + "\" deleted successfully");
        }

    }

    public void close() {
        try {
            writer.close();
            if(reader != null) reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}